package co.ealarcon.carsdisplayer.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import co.ealarcon.carsdisplayer.R
import co.ealarcon.carsdisplayer.first
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Screen tests for the Main Activity
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activity = ActivityTestRule(MainActivity::class.java)

    /**
     * Tests the item expansion in list fragment
     */
    @Test
    fun expandItem() {
        Thread.sleep(3000)
        onView(first(withId(R.id.title_container))).perform(click())
        onView(first(withId(R.id.details_container))).check(matches(isDisplayed()))
    }

    /**
     * Tests the action to go to map fragment from list fragment
     */
    @Test
    fun goToMap() {
        Thread.sleep(3000)
        onView(first(withId(R.id.title_container))).perform(click())
        onView(first(withId(R.id.action_button))).perform(click())
        Thread.sleep(3000)
        onView(withId(R.id.map_view)).check(matches(isDisplayed()))
    }

}