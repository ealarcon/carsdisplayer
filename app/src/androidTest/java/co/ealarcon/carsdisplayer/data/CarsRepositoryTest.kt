package co.ealarcon.carsdisplayer.data

import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Test
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Tests the features in CarsRepository
 *
 */
class CarsRepositoryTest {
    /**
     * Tests the car fetching function with a mocked response
     */
    @Test
    fun testFetchCars() {
        // given
        val repository = spyk<CarsRepository>()
        val retrofit = mockk<Retrofit>()
        val service = mockk<CarsAPIInterface>()
        val call = mockk<Call<List<CarEntity>?>>()
        val response = mockk<Response<List<CarEntity>?>>()
        val carEntity = mockk<CarEntity>()
        every { retrofit.create(CarsAPIInterface::class.java) } returns service
        every { carEntity.id } returns "1"
        every { response.body() } returns listOf(carEntity)
        every { response.isSuccessful } returns true
        every { call.execute() } returns response
        every { service.getCars() } returns call
        every { repository.getService() } returns service

        // when
        val result = repository.fetchCarsList()

        // then
        verify { service.getCars() }
        verify { repository.fetchCarsList() }
        assertEquals(result?.get(0)?.id, "1")

    }

}