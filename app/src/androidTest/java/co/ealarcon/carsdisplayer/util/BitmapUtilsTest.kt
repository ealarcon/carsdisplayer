package co.ealarcon.carsdisplayer.util

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import co.ealarcon.carsdisplayer.R

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.assertEquals

/**
 * Tests the features in BitmapUtils
 *
 */
@RunWith(AndroidJUnit4::class)
class BitmapUtilsTest {
    /**
     * Tests the bitmap resize function
     */
    @Test
    fun resizeBitmapTest() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val resizedBitmap = resizeBitmapFromResource(
            context = appContext,
            width = 100,
            height = 100,
            resourceId = R.drawable.bmw_default_image
        )
        assertEquals(resizedBitmap.width, 100)
        assertEquals(resizedBitmap.height, 100)
    }
}
