package co.ealarcon.carsdisplayer.ui.map

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.ealarcon.carsdisplayer.R
import co.ealarcon.carsdisplayer.data.CarEntity
import co.ealarcon.carsdisplayer.data.CustomInfoWindowData
import co.ealarcon.carsdisplayer.ui.CarsDisplayerViewModel
import co.ealarcon.carsdisplayer.util.resizeBitmapFromResource
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

/**
 * Fragment that shows the cars in their locations on a map
 */
class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var gmap: GoogleMap
    private lateinit var mapView: MapView
    private lateinit var viewModel: CarsDisplayerViewModel
    private lateinit var fromCarIdSelected: String
    private var selectedMarker: Marker? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = requireActivity().let {
            ViewModelProvider(it).get(CarsDisplayerViewModel::class.java)
        }
        (viewModel.fragmentReady as MutableLiveData).value = false

        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        fromCarIdSelected = MapFragmentArgs.fromBundle(requireArguments()).id
        mapView = view.findViewById(R.id.map_view) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        gmap = googleMap
        gmap.setInfoWindowAdapter(CustomInfoWindowAdapter(requireContext()))
        viewModel.cars.observe(this,
            Observer<List<CarEntity>> { carsList ->
                (viewModel.fragmentReady as MutableLiveData).value = true
                // Get the custom car marker resized for the map according to device display density
                val desiredSize = when (resources.displayMetrics.densityDpi) {
                    DisplayMetrics.DENSITY_LOW -> 25
                    DisplayMetrics.DENSITY_MEDIUM, DisplayMetrics.DENSITY_HIGH -> 50
                    else -> 100
                }
                val customMarkerIconResized = resizeBitmapFromResource(
                    context = requireContext(),
                    width = desiredSize,
                    height = desiredSize,
                    resourceId = R.drawable.car_marker
                )
                // Add markers for every car in the list and check if we have a selected marker
                carsList?.forEach { car ->
                    val location = LatLng(car.latitude, car.longitude)
                    val markerOptions = MarkerOptions()
                        .position(location)
                        .title(car.name)
                        .icon(
                            BitmapDescriptorFactory.fromBitmap(customMarkerIconResized)
                        )
                    val viewInfoData = CustomInfoWindowData(
                        carImageUrl = car.carImageUrl,
                        modelName = car.modelName,
                        licensePlate = car.licensePlate,
                        color = car.color
                    )
                    // If we come from list view action to show car in map we show the info window
                    if (fromCarIdSelected == car.id) {
                        selectedMarker = gmap.addMarker(markerOptions)
                        selectedMarker?.tag = viewInfoData
                        selectedMarker?.showInfoWindow()
                    } else {
                        gmap.addMarker(markerOptions).tag = viewInfoData
                    }

                }
                // Zoom in 10 times into the marker area if we don't have a selected marker
                // and if we have a selected marker zoom into it 12 times
                if (!carsList.isNullOrEmpty()) {
                    val selectedMarker = selectedMarker
                    if (selectedMarker != null) {
                        gmap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    selectedMarker.position.latitude,
                                    selectedMarker.position.longitude
                                ),
                                12F
                            )
                        )
                    } else {
                        gmap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    carsList[0].latitude,
                                    carsList[0].longitude
                                ),
                                10F
                            )
                        )
                    }
                }
            })
    }
}