package co.ealarcon.carsdisplayer.ui

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import co.ealarcon.carsdisplayer.R
import co.ealarcon.carsdisplayer.data.CarEntity
import com.google.android.material.bottomnavigation.BottomNavigationView

const val TAG = "CarsDisplayer"

/**
 * Main activity that contains two tabs: One showing the list of cars and one showing their location
 * on a map.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: CarsDisplayerViewModel
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        progressBar = findViewById(R.id.progress)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_list,
                R.id.navigation_map
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        viewModel = ViewModelProvider(this).get(CarsDisplayerViewModel::class.java)

        // If the cars list gets updated to null it means there was an error loading it
        viewModel.cars.observe(this,
            Observer<List<CarEntity>> { carsList ->
                if (carsList == null) {
                    Toast.makeText(this, getString(R.string.fetch_list_error), Toast.LENGTH_LONG)
                        .show()
                }
            })

        // Update the progress bar according to the fragments statuses
        viewModel.fragmentReady.observe(this,
            Observer<Boolean> { fragmentReady ->
                if (fragmentReady) {
                    progressBar.visibility = View.GONE
                } else {
                    progressBar.visibility = View.VISIBLE
                }
            })
    }
}
