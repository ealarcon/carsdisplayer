package co.ealarcon.carsdisplayer.ui.map

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import co.ealarcon.carsdisplayer.R
import co.ealarcon.carsdisplayer.data.CustomInfoWindowData
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * InfoWindowAdapter implementation to customize InfoWindow for car markers on map
 */
class CustomInfoWindowAdapter(val context: Context) : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker): View? = null

    override fun getInfoContents(marker: Marker): View {
        val view: View = (context as AppCompatActivity).layoutInflater
            .inflate(R.layout.custom_info_window, null)

        val image: AppCompatImageView = view.findViewById(R.id.image)
        val progressBar: ProgressBar = view.findViewById(R.id.progress)
        val name: AppCompatTextView = view.findViewById(R.id.name)
        val modelName: AppCompatTextView = view.findViewById(R.id.model_name)
        val licensePlate: AppCompatTextView = view.findViewById(R.id.license_plate)
        val color: AppCompatTextView = view.findViewById(R.id.color)

        val infoWindowData: CustomInfoWindowData = marker.tag as CustomInfoWindowData

        name.text = marker.title
        progressBar.visibility = View.VISIBLE
        Picasso.get().load(infoWindowData.carImageUrl).error(R.drawable.bmw_default_image)
            .into(image, object : Callback {
                override fun onSuccess() {
                    progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    progressBar.visibility = View.GONE
                }

            })
        modelName.text = infoWindowData.modelName
        licensePlate.text = infoWindowData.licensePlate
        color.text = infoWindowData.color

        return view
    }

}