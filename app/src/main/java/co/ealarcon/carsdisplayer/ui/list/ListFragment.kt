package co.ealarcon.carsdisplayer.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import co.ealarcon.carsdisplayer.R
import co.ealarcon.carsdisplayer.data.CarEntity
import co.ealarcon.carsdisplayer.ui.CarsDisplayerViewModel

/**
 * Fragment that shows the list of cars with their details
 */
class ListFragment : Fragment() {

    private lateinit var viewModel: CarsDisplayerViewModel
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: CarsListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = requireActivity().let {
            ViewModelProvider(it).get(CarsDisplayerViewModel::class.java)
        }
        (viewModel.fragmentReady as MutableLiveData).value = false
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        swipeRefresh = view.findViewById(R.id.swipe_container)
        swipeRefresh.setOnRefreshListener {
            viewModel.loadCars()
        }
        recyclerView = view.findViewById(R.id.cars_list)
        adapter = CarsListAdapter(requireContext(), emptyList<CarEntity>().toMutableList())
        recyclerView.adapter = adapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.cars.observe(this,
            Observer<List<CarEntity>> { carsList ->
                swipeRefresh.isRefreshing = false
                (viewModel.fragmentReady as MutableLiveData).value = true
                adapter.clear()
                adapter.addAll(carsList)
            })
    }
}