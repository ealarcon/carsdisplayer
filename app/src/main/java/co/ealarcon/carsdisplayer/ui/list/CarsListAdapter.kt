package co.ealarcon.carsdisplayer.ui.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import co.ealarcon.carsdisplayer.R
import co.ealarcon.carsdisplayer.data.CarEntity
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * Adapter for the cars list
 */
class CarsListAdapter(private val context: Context, private val carsList: MutableList<CarEntity>) :
    RecyclerView.Adapter<CarsListAdapter.CarViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CarViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.list_item_car, parent, false)
        return CarViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: CarViewHolder,
        position: Int
    ) {
        val car = carsList[position]

        holder.bind(car)
        holder.itemView.setOnClickListener {
            val expanded = car.expanded
            car.expanded = !expanded
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return carsList.size
    }

    /**
     * Clears all elements in the list
     */
    fun clear() {
        carsList.clear()
        notifyDataSetChanged()
    }

    /**
     * Adds a list of elements
     */
    fun addAll(list: List<CarEntity>) {
        carsList.addAll(list)
        notifyDataSetChanged()
    }

    /**
     * ViewHolder that represents a car item in the cars list
     */
    inner class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val progressBar: ProgressBar = itemView.findViewById(R.id.progress)
        private val image: AppCompatImageView = itemView.findViewById(R.id.image)
        private val name: AppCompatTextView = itemView.findViewById(R.id.name)
        private val expandButton: AppCompatImageView = itemView.findViewById(R.id.expand_button)
        private val detailsContainer: View = itemView.findViewById(R.id.details_container)
        private val modelName: AppCompatTextView = itemView.findViewById(R.id.model_name)
        private val licensePlate: AppCompatTextView = itemView.findViewById(R.id.license_plate)
        private val color: AppCompatTextView = itemView.findViewById(R.id.color)
        private val actionButton: AppCompatButton = itemView.findViewById(R.id.action_button)

        fun bind(car: CarEntity) {
            progressBar.visibility = View.VISIBLE
            Picasso.get().load(car.carImageUrl).error(R.drawable.bmw_default_image)
                .into(image, object : Callback {
                    override fun onSuccess() {
                        progressBar.visibility = View.GONE
                    }

                    override fun onError(e: Exception?) {
                        progressBar.visibility = View.GONE
                    }

                })
            name.text = car.name
            modelName.text = car.modelName
            licensePlate.text = car.licensePlate
            color.text = car.color
            if (car.expanded) {
                detailsContainer.visibility = View.VISIBLE
                expandButton.setImageResource(R.drawable.ic_expand_less)
            } else {
                detailsContainer.visibility = View.GONE
                expandButton.setImageResource(R.drawable.ic_expand_more)
            }
            actionButton.setOnClickListener {
                val bundle = bundleOf("id" to car.id)
                itemView.findNavController().navigate(R.id.action_go_to_marker, bundle)
            }
        }
    }
}