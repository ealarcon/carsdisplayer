package co.ealarcon.carsdisplayer.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.ealarcon.carsdisplayer.data.CarEntity
import co.ealarcon.carsdisplayer.data.CarsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * ViewModel to share the car info between activities and fragments
 */
class CarsDisplayerViewModel: ViewModel() {

    init {
        loadCars()
    }

    private val _cars = MutableLiveData<List<CarEntity>>()
    /**
     * Cars list livedata to be observed
     */
    val cars: LiveData<List<CarEntity>> = _cars

    val fragmentReady: LiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }

    /**
     * Loads the cars list from API using the repository
     */
    fun loadCars() {
        viewModelScope.launch(Dispatchers.Default) {
            val result = CarsRepository.getInstance().fetchCarsList()
            withContext(Dispatchers.Main) {
                _cars.value = result
            }
        }
    }
}