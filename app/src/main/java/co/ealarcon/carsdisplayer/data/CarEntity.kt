package co.ealarcon.carsdisplayer.data

const val DEFAULT_CAR_IMAGE = "https://www.bmw.com.co/content/dam/bmw/common/all-models/4-series/gran-coupe/2017/navigation/BMW-4-Series-Gran-Coupe-ModelCard.png"

/**
 * Entity class that holds the car information
 */
data class CarEntity(val id: String,
                     val modelIdentifier: String,
                     val modelName: String,
                     val name: String,
                     val make: String,
                     val group: String,
                     val color: String,
                     val series: String,
                     val fuelType: String,
                     val fuelLevel: Double,
                     val transmission: String,
                     val licensePlate: String,
                     val latitude: Double,
                     val longitude: Double,
                     val innerCleanliness: String,
                     val carImageUrl: String = DEFAULT_CAR_IMAGE,
                     var expanded: Boolean = false)
