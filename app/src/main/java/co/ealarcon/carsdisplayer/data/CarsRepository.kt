package co.ealarcon.carsdisplayer.data

import android.util.Log
import androidx.annotation.WorkerThread
import co.ealarcon.carsdisplayer.ui.TAG
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Repository that encapsulates the data fetching operations
 */
class CarsRepository {

    /**
     * Gets the API service from retrofit
     */
    fun getService(): CarsAPIInterface {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(CarsAPIInterface.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create<CarsAPIInterface>(CarsAPIInterface::class.java)
    }

    /**
     * Fetches the cars list from API
     */
    @WorkerThread
    fun fetchCarsList(): List<CarEntity>? {

        val response = getService().getCars().execute()

        return if (response.isSuccessful) {
            response.body()
        } else {
            Log.e(TAG, "Error fetching the cars list. ${response.errorBody()?.string()}")
            null
        }
    }

    companion object {
        private var INSTANCE: CarsRepository? = null
        fun getInstance() = INSTANCE
            ?: CarsRepository().also {
                INSTANCE = it
            }
    }
}