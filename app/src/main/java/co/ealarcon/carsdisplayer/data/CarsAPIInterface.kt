package co.ealarcon.carsdisplayer.data

import retrofit2.Call
import retrofit2.http.GET

/**
 * API interface to retrieve the cars information
 */
interface CarsAPIInterface {
    /**
     * Loads the cars list
     */
    @GET("codingtask/cars")
    fun getCars(): Call<List<CarEntity>?>

    companion object {
        const val BASE_URL = "https://cdn.sixt.io/"
    }
}