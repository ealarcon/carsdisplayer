package co.ealarcon.carsdisplayer.data

/**
 * Data class that holds InfoWindow data for the CustomInfoWindowAdapter to consume
 */
data class CustomInfoWindowData(
    val carImageUrl: String?,
    val modelName: String?,
    val licensePlate: String?,
    val color: String?
)