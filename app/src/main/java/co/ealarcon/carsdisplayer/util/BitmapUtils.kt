package co.ealarcon.carsdisplayer.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import androidx.annotation.DrawableRes

/**
 * Utilities functions for handling Bitmaps
 */

/**
 * Resizes a Bitmap from a given resource to the given size
 */
fun resizeBitmapFromResource(
    context: Context,
    width: Int,
    height: Int,
    @DrawableRes resourceId: Int
): Bitmap {
    val bitmapDrawable = context.resources.getDrawable(resourceId, null) as BitmapDrawable
    val bitmapOriginal = bitmapDrawable.bitmap
    return Bitmap.createScaledBitmap(bitmapOriginal, width, height, false)
}