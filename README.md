# CarsDisplayer

Coding Task to show a list of cars on a map and a list.

## Download

You can get the APK from [here](https://drive.google.com/open?id=1NJl2fkVfoJSxhFBr6HPqY08ZZwVcwHC).

## Libraries used

CarsDisplayer uses [Retrofit](https://square.github.io/retrofit/) for API calls, [Picasso](https://square.github.io/picasso/) for image loading because of its ease of use and image caching feature and [GoogleMaps SDK for Android](https://developers.google.com/maps/documentation/android-sdk/intro).

## Usage

![](http://prueba.ealarcon.webfactional.com/wp-content/uploads/2020/02/carsdisplayer-demo.gif)